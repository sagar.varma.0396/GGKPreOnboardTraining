﻿using System;

namespace PrimeFibonacci
{

    class Program
    {
        /*Driver class to accept Input and printPatern
         */

        static void Main(string[] args)
        {
            FibonacciPrime primeFibonacci = new FibonacciPrime();

            Console.WriteLine("Enter Fibonacci Length: ");

            primeFibonacci.FibonaciiLength = Convert.ToInt32(Console.ReadLine());

            primeFibonacci.PrintPattern();
        }
    }
}
