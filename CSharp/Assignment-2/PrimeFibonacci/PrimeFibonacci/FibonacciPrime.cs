﻿using System;
using System.Collections.Generic;

namespace PrimeFibonacci
{
    class FibonacciPrime
    {
        /*class to represent numbers in fibonacci that are prime
         * it also stores the length of fibonacci series
         * it has operation to print fibonacci prime pattern
         */

        private IList<long> _myPrimeList = new List<long>();
        private int _fibonaciiLength;

        public int FibonaciiLength
        {
            get
            {
                return _fibonaciiLength;
            }
            set
            {
                this._fibonaciiLength = value;
            }
        }
        private bool IsPrime(long number)
        {
            /*function to check if a given number is prime or not
             * it return bool
             */

            for (long i = 2; i * i <= number; i++)
            {
                if (number % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

        private void PopulatePrimeFibonacci()
        {
            /*function to populate primefibonacci list
             * it stores prime numbers in _myPrimeList
             */
             
            long firstNumber = 1;
            long secondNumber = 1;
            long temp;

            for (int i = 1; i < FibonaciiLength; i++)
            {
                temp = firstNumber;
                firstNumber = secondNumber;
                secondNumber = temp + secondNumber;

                if (IsPrime(secondNumber))
                {
                    _myPrimeList.Add(secondNumber);
                }
            }

        }

        public void PrintPattern()
        {
            /*function to print fibonacci prime patter
             */

            PopulatePrimeFibonacci();

            for (int i = 0; i < _myPrimeList.Count; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Console.Write(_myPrimeList[j] + " ");
                }

                Console.WriteLine();
            }
        }
    }
}
