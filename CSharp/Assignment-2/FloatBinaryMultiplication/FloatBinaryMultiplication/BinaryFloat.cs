﻿namespace FloatBinaryMultiplication
{
    class BinaryFloat
    {
        /*class to represent binary floating point numbers
         * integral part refers to digits before point
         * fractional part refers to digits after point
         */

        private string _integralPart;
        private string _fractionalPart;

        public BinaryFloat()
        {

        }

        public BinaryFloat(string integralPart, string decimalPart)
        {
            this._integralPart = integralPart;
            this._fractionalPart = decimalPart;
        }
        
        public string IntegralPart
        {
            get
            {
                return _integralPart;
            }
            set
            {
                this._integralPart = value;
            }
        }

        public string FractionalPart
        {
            get
            {
                return _fractionalPart;
            }
            set
            {
                this._fractionalPart = value;
            }
        }

        public override string ToString()
        {
            return _integralPart + "." + _fractionalPart;
        }
    }
}
