﻿using System;

namespace FloatBinaryMultiplication
{
    class Program
    {

        static float Multiply(float firstNumber, float secondNumber)
        {
            /*function to call respective methods to multiply floats
             * it converts two float into its binary representation
             * and multiply them using helper methods
             */

            BinaryFloat firstBinaryFloat = BinaryFloatHelper.GetBinaryFloat(firstNumber);
            BinaryFloat secondBinaryFloat = BinaryFloatHelper.GetBinaryFloat(secondNumber);

            Console.WriteLine("FirstNumber Binary representation: " + firstBinaryFloat);
            Console.WriteLine("SecondNumber binary respresentation: " + secondBinaryFloat);

            BinaryFloat result = BinaryFloatHelper.Multiply(firstBinaryFloat, secondBinaryFloat);

            Console.WriteLine("Result in BinaryFloat: " + result);

            return BinaryFloatHelper.GetFloat(result);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter two floating Point Numbers: ");

            float firstNumber = (float)Convert.ToDouble(Console.ReadLine());
            float secondNumber = (float)Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Mutiplication Result: "+ Multiply(firstNumber, secondNumber));
            Console.ReadKey();
        }
    }
}
