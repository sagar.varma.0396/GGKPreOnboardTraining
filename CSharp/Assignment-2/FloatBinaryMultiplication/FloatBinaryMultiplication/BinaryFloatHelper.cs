﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FloatBinaryMultiplication
{
    class BinaryFloatHelper
    {
        /*class to facilitate multiplication of two binary floating point numbers
         * constructor is private to not allow object creation
         * it consists of only static methods
         */
         
        private BinaryFloatHelper()
        {

        }

        public static BinaryFloat GetBinaryFloat(float number)
        {
            /*
             * function to return floating point binary*
             * it takes float as a parameter
             */

            StringBuilder integralResult = new StringBuilder();
            StringBuilder fractionalResult = new StringBuilder();

            int integral = (int)number;

            float fractional = number - integral;

            if(integral == 0)
            {
                integralResult.Append("0");
            }

            while (integral != 0)
            {
                integralResult.Insert(0, integral % 2);
                integral = integral / 2;
            }

            if(fractional == 0)
            {
                fractionalResult.Append("0");
            }

            while (fractional != 0)
            {
                fractional = fractional * 2;

                if (fractional >= 1)
                {
                    fractionalResult.Append("1");
                    fractional = fractional - 1;
                }
                else
                {
                    fractionalResult.Append("0");
                }
            }

            

            return new BinaryFloat(integralResult.ToString(), fractionalResult.ToString());
        }

        public static float GetFloat(BinaryFloat binaryFloat)
        {
            /*
             * function to return float of a binary floating point
             * it takes binary float as a parameter
             */

            int integralResult = 0;
            float fractionalResult = 0;

            string integralPart = binaryFloat.IntegralPart;
            string fractionalPart = binaryFloat.FractionalPart;

            int count = 0;

            for (int i = integralPart.Length - 1; i >= 0; i--)
            {
                integralResult = integralResult + Convert.ToInt32(integralPart[i] + "") * (int)Math.Pow(2, count);
                count++;
            }

            count = -1;

            for(int i = 0; i < fractionalPart.Length; i++)
            {
                fractionalResult = fractionalResult + Convert.ToInt32(fractionalPart[i]+"") * (float)Math.Pow(2,count);
                count--;
            }
            
            return integralResult + fractionalResult;
        }

        private static string Add(string first, string second)
        {
            /*function to add two binary strings
             * it takes 2 binary string as a parameter
             */

            int i = first.Length - 1;
            int j = second.Length - 1;
            int carry = 0;
            int sum = 0;
            StringBuilder result = new StringBuilder();

            while(i>=0 || j>=0 || carry == 1)
            {
                sum = carry;

                if (i >= 0)
                {
                    sum = sum + (first[i] - '0');
                }
                if (j >= 0)
                {
                    sum = sum + (second[j]- '0');
                }
                
                carry = sum / 2;
                result.Insert(0, sum % 2);
                i--;j--;
            }

            return result.ToString();
        }

        private static string AddList(IList<string> list)
        {
            /*
             * function to add List consisting of all binary strings
             * it takes generic list as a parameter
             */

            string result = list[0]; 
            for(int i = 1; i < list.Count; i++)
            {
                result = Add(result, list[i]);
            }
            return result;
        }

        public static BinaryFloat Multiply(BinaryFloat firstNumber, BinaryFloat secondNumber)
        {
            /*function to multiply to binary floating point
             * it takes two binary float as a parameter
             */

            string firstBinary = firstNumber.IntegralPart + firstNumber.FractionalPart;
            string secondBinary = secondNumber.IntegralPart + secondNumber.FractionalPart;
            IList<string> result = new List<string>();

            StringBuilder temp = new StringBuilder();

            for(int i = secondBinary.Length - 1; i >= 0; i--)
            {
                temp.Clear();
                int first = Convert.ToInt32(secondBinary[i] + "");

                for(int j = secondBinary.Length - 1; j > i; j--)
                {
                    temp.Append("0");
                }

                for(int j = firstBinary.Length - 1; j >= 0; j--)
                { 
                    temp.Insert(0,first * Convert.ToInt32(firstBinary[j] + ""));
                }

                result.Add(temp.ToString());
            }

            string addResult = AddList(result);
            int decimalPointIndex = (secondNumber.FractionalPart.Length + firstNumber.FractionalPart.Length);
            return new BinaryFloat(addResult.Substring(0,addResult.Length-decimalPointIndex), addResult.Substring(addResult.Length-decimalPointIndex,decimalPointIndex));
        }
    }
}
