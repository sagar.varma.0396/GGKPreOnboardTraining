﻿using System;

namespace ConsoleApp1
{
    class Node
    {
        private int _data;
        private Node _left;
        private Node _right;

        public Node()
        {
        }

        public Node(int data)
        {
            this._data = data;
        }

        public Node(int data, Node left, Node right)
        {
            this._data = data;
            this._left = left;
            this._right = right;
        }

        public int Data
        {
            get
            {
              return this._data;
            }

            set
            {
                this._data = value;
            }
        }

        public Node Left
        {
            get
            {
                return _left;
            }
            set
            {
                this._left = value;

            }
        }
        
        public Node Right
        {
            get
            {
                return _right;
            }
            set
            {
                this._right = value;
            }
        }
    }

    class BinarySearchTree
    {
        Node _root;

        Node GetMin(Node root)
        {
            if(root.Left == null)
            {
                return root;
            }

            return GetMin(root.Left);
        }

        Node Delete(Node root,int data)
        {
            if(root == null)
            {
                Console.WriteLine("Element Not Found!");
                return root;
            }

            if(root.Data > data)
            {
                root.Left = Delete(root.Left, data);
            }
            else if(root.Data < data)
            {
                root.Right = Delete(root.Right, data);
            }
            else
            {
                if(root.Left == null)
                {
                    root = root.Right;

                }else if(root.Right == null)
                {
                    root = root.Left;
                }
                else
                {
                    Node min = GetMin(root.Right);
                    root.Data = min.Data;
                    root.Right = Delete(root.Right,min.Data);
                }
            }

            return root;
        }

        Node Insert(Node root, int data)
        {
            if (root == null)
            {
                return new Node(data);
            }

            if (root.Data > data)
            {
                root.Left = Insert(root.Left, data);
            }
            else if (root.Data < data)
            {
                root.Right = Insert(root.Right, data);
            }
            return root;
        }

        public void InorderTraversal(Node root)
        {
            if (root == null)
            {
                return;
            }

            InorderTraversal(root.Left);
            Console.Write(root.Data + " ");
            InorderTraversal(root.Right);
        }

        public void PreorderTraversal(Node root)
        {
            if (root == null)
            {
                return;
            }

            Console.Write(root.Data + " ");
            PreorderTraversal(root.Left);
            PreorderTraversal(root.Right);
            
        }

        public void PostorderTraversal(Node root)
        {
            if (root == null)
            {
                return;
            }

            PostorderTraversal(root.Left);
            PostorderTraversal(root.Right);
            Console.Write(root.Data + " ");
        }

        public void Display()
        {
            if(_root == null)
            {
                Console.WriteLine("Tree Empty");
                return;
            }

            InorderTraversal(_root);
            Console.WriteLine();
            PreorderTraversal(_root);
            Console.WriteLine();
            PostorderTraversal(_root);
            Console.WriteLine();
        }

        public void Insert(int data)
        {
            _root = Insert(_root, data);
        }

        public void Delete(int data)
        {
            _root = Delete(_root, data);
        }

        public void AscendingOrder(Node root)
        {
            if(root == null)
            {
                return;
            }

            AscendingOrder(root.Left);
            Console.Write(root.Data+" ");
            AscendingOrder(root.Right);
        }

        public void DescendingOrder(Node root)
        {
            if (root == null)
            {
                return;
            }

            DescendingOrder(root.Right);
            Console.Write(root.Data + " ");
            DescendingOrder(root.Left);
        }

        int Search(Node root, int data, int level)
        {
            if(root == null)
            {
                return -1;
            }

            if(root.Data > data)
            {
                return Search(root.Left, data, level+1);
            }
            else if(root.Data < data)
            {
                return Search(root.Right, data, level+1);
            }

            return level;
        }

        public void Search(int data)
        {
            int level = Search(_root, data , 0);

            if(level == -1)
            {
                Console.WriteLine(data + " Not Found");
                return;
            }

            Console.WriteLine(data + "Found at level " + level);
        }
        public void AscendingOrder()
        {
            AscendingOrder(_root);
        }

        public void DescendingOrder()
        {
            DescendingOrder(_root);
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            BinarySearchTree bst = new BinarySearchTree();

            bst.Display();

            bst.Insert(10);
            bst.Insert(20);
            bst.Insert(30);
            bst.Insert(5);
            bst.Insert(15);
            bst.Insert(1);

            bst.Display();

            bst.Delete(1);
            bst.Display();
            bst.Delete(55);
            bst.AscendingOrder();
            bst.DescendingOrder();

        }
    }
}
