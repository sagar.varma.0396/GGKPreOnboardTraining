﻿using System;


namespace ConsoleApp3
{
    class Program
    {
        static bool IsPrime(int number)
        {
            for(int i = 2; i * i <= number; i++)
            {
                if (number % i == 0)
                {
                    return false;
                }
            }

            return true;
        }

        static bool IsPrimeSum(int number)
        {
            int sum = 2;

            for(int i = 3; i < number; i = i + 2)
            {
                if (sum > number)
                {
                    return false;
                }

                if (IsPrime(i))
                {
                    sum = sum + i;
                }

                if (sum == number)
                {
                    return true;
                }
            }

            return false;
        }

        static int GetConsecutivePrimeSumCount(int limit)
        {
            int count = 0;

            for(int i = 3; i <= limit; i++)
            {
                if (IsPrime(i) && IsPrimeSum(i))
                {
                    count = count + 1;
                }
            }

            return count;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter The Limit: ");
            int limit = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(GetConsecutivePrimeSumCount(limit));
        }
    }
}
