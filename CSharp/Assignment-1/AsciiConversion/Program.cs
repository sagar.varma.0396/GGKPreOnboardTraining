﻿using System;
using System.Text;

namespace ConsoleApp2
{

    class Program
    {
        static int GetAverage(int num1, int num2)
        {
            int avg = (num1 + num2) / 2;
            return avg;
        }

        static void DisplayAscii(string String)
        {
            for(int i = 0; i < String.Length; i++)
            {
                Console.Write((int)String[i] + " ");
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            string String = Console.ReadLine();
            String = GetEncodedString(String);
            Console.Write("Before Checking Prime: ");
            DisplayAscii(String);
            Console.Write("After Checking Prime: ");
            String = CheckAndRemovePrime(String);
            DisplayAscii(String);
            Console.WriteLine("After Conversion: " + String);
        }

        static string GetEncodedString(string String)
        {
            StringBuilder encodedString = new StringBuilder();

            for(int i = 0; i < String.Length-1; i++)
            {
                int avg = GetAverage(String[i], String[i + 1]);

                encodedString.Append((char)avg);
            }

            return encodedString.ToString();
        }

        static bool IsPrime(int n)
        {
            for(int i = 2; i * i <= n; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }

            return true;
        }

        static string CheckAndRemovePrime(string String)
        {
            StringBuilder result = new StringBuilder();

            for(int i = 0; i < String.Length; i++)
            {
                int current = String[i];

                if (IsPrime(current))
                {
                    current = current + 1;
                }

                result.Append((char)current);
            }

            return result.ToString();
        }
    }
}
