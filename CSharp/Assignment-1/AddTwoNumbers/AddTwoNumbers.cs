﻿using System;
using System.Text;


namespace ConsoleApp1
{
    class AddTwoNumbers
    {
        public AddTwoNumbers()
        {
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter first Integer: ");
            string firstNumber = Console.ReadLine();
            Console.WriteLine("Enter Second Integer: ");
            string secondNumber = Console.ReadLine();
            Console.WriteLine(Add(firstNumber, secondNumber));
        }

        static string Add(string FirstNumber, string SecondNumber)
        {
            StringBuilder result = new StringBuilder();
            int sum, carry = 0;


            for(int i=FirstNumber.Length-1, j=SecondNumber.Length-1; i>=0 || j>=0; i--, j--)
            {
                sum = ((i >= 0) ? (FirstNumber[i] -'0') : 0) + ((j >= 0) ? (SecondNumber[i] - '0') : 0) + carry;

                carry = (sum > 9) ? 1 : 0;

                sum = sum % 10;

                result.Insert(0, sum);
            }

            if(carry == 1)
            {
                result.Insert(0, 1);
            }

            return result.ToString();
        }

         
    }
}

