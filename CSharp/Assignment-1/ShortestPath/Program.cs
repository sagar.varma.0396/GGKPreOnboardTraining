﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShortestPath
{
    class Node
    {
        private int _destination;
        private int _weight;
        private Node _next;

        Node() { }

        public Node(int destination, int weight)
        {
            this._destination = destination;
            this._weight = weight;
        }

        public int Destination
        {
            get
            {
                return _destination;
            }

            set
            {
                this._destination = value;
            }
        }

        public int Weight
        {
            get
            {
                return _weight;
            }
            set
            {
                this._weight = value;
            }
        }

        public Node Next
        {
            get
            {
                return _next;
            }
            set
            {
                this._next = value;
            }
        }
    }

    class MyData
    {
        public int Vertex;
        public int Distance;
        public bool Visited;
        public int Parent;
    }

    class Graph
    {
        private int _numberOfVertices;
        private IDictionary<int,Node> _myGraph = new Dictionary<int,Node>();
        

        public int NumberOfVertices
        {
            get
            {
                return this._numberOfVertices;
            }
            set
            {
                this._numberOfVertices = value;
            }
        }

        public void AddVertex(int VertexIdentifier)
        {
            if (_myGraph.ContainsKey(VertexIdentifier))
            {
                Console.WriteLine("Vertex " + VertexIdentifier + "Already Added");
                return;
            }
            
            _myGraph.Add(VertexIdentifier, null);
        }

        public void AddEdge(int source, int destination, int weight)
        {
            Node newNode = new Node(destination, weight);

            if (!_myGraph.ContainsKey(source))
            {
                Console.WriteLine("Vertex " + source + "NotFound");
                return;
            }

            Node temp = _myGraph[source];

            if (temp != null)
            {
                newNode.Next = temp;
            }    
            
            _myGraph[source] = newNode;

            temp = _myGraph[destination];

            newNode = new Node(source,weight);

            if (temp != null)
            {
                newNode.Next = temp;
            }

            _myGraph[destination] = newNode;
        }

        public void ShortestDistancePath(int source,int destination)
        {
            MyData[] distanceFromSource = new MyData[_numberOfVertices];
            IDictionary<int, int> map = new Dictionary<int, int>();
           
            
            for(int i = 0; i < _myGraph.Count; i++)
            {
                distanceFromSource[i] = new MyData();
                distanceFromSource[i].Vertex = _myGraph.Keys.ElementAt(i);
                map.Add(distanceFromSource[i].Vertex, i);
                distanceFromSource[i].Distance = Int32.MaxValue;
                distanceFromSource[i].Visited = false;
                distanceFromSource[i].Parent = -1;
            }

            distanceFromSource[map[source]].Distance = 0;

            for (int i = 0; i < _numberOfVertices; i++)
            {
                int minVertex = GetMinimumVertex(distanceFromSource);
                if (minVertex == destination)
                {
                    break;
                }
                
                if (map.ContainsKey(minVertex))
                distanceFromSource[map[minVertex]].Visited = true;


                Node temp = _myGraph[minVertex];

                MyData current = null;

                while (temp != null)
                {
                    current = distanceFromSource[map[temp.Destination]];
                    
                    if(current.Visited == false && current.Distance > temp.Weight + distanceFromSource[map[minVertex]].Distance)
                    {
                        current.Distance = temp.Weight + distanceFromSource[map[minVertex]].Distance;
                        current.Parent = minVertex;
                        distanceFromSource[map[temp.Destination]] = current;
                    }

                    temp = temp.Next;
                }
                
            }
            Console.Write("The Shortest Path is ");
            PrintPath(distanceFromSource,distanceFromSource[map[destination]].Parent, map);
            Console.WriteLine(destination + " With Total Distance Of " + distanceFromSource[map[destination]].Distance);
        }

        private void PrintPath(MyData[] myData, int destination, IDictionary<int,int> map)
        {
            if(myData[map[destination]].Parent == -1)
            {
                Console.Write(myData[map[destination]].Vertex + "->");
                return;
            }

            PrintPath(myData, myData[map[destination]].Parent, map);

            Console.Write(myData[map[destination]].Vertex + "->");
        }

        private int GetMinimumVertex(MyData[] myData)
        {
            int min = Int32.MaxValue;
            int minIndex = 0;

            for(int i = 0; i < _numberOfVertices ; i++)
            {
                if(myData[i].Visited == false && myData[i].Distance <= min)
                {
                    min = myData[i].Distance;
                    minIndex = myData[i].Vertex;
                }
            }

            return minIndex;
        }       
    }


    class Problem
    {
        static void Main(string[] args)
        {
            Graph graph = new Graph();

            Console.WriteLine("Enter Number Of Vertex ");
            graph.NumberOfVertices = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter " + graph.NumberOfVertices + " vertices identifier ");

            for(int i=0; i < graph.NumberOfVertices; i++)
            {
                graph.AddVertex(Convert.ToInt32(Console.ReadLine()));
            }

            Console.WriteLine("Enter Number Of Edges ");

            int numberOfEdges = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Source Destination Vertex of each edge in different Lines: ");


            for(int i = 0; i < numberOfEdges; i++)
            {
                int source = Convert.ToInt32(Console.ReadLine());
                int destination = Convert.ToInt32(Console.ReadLine());
                int weight = Convert.ToInt32(Console.ReadLine());
                graph.AddEdge(source, destination, weight);
            }

            Console.WriteLine("Enter Source Vertex And Destination vertex in different Lines: ");

            int sourceVertex = Convert.ToInt32(Console.ReadLine());
            int destinationVertex = Convert.ToInt32(Console.ReadLine());
            graph.ShortestDistancePath(sourceVertex, destinationVertex);
        }
    }
}
